
## terraform block 

terraform {
    required_version = ">= 1.1.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}


# Provider Block
provider "aws" {
  profile = "cli" # AWS Credentials Profile configured on your local desktop terminal  $HOME/.aws/credentials
  region  = "us-east-1"
}


# Create a VPC
resource "aws_vpc" "demo" {
  cidr_block = "10.5.0.0/16"
  tags = {
    Name = "demo_vpc"
  }
}


# creating EC2
          #local_name    #resource_name
resource "aws_instance" "ec2_instance" {
  ami           = var.ami_id
  instance_type = "t2.micro"

  tags = {
    Name = "demo_instance"
  }
}

